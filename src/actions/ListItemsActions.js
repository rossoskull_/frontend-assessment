import axios from 'axios'

// Types
export const ADD_ITEM = 'list:add_list_item'
export const DELETE_ITEM = 'list:delete_list_item'
export const ADD_ALL_ITEMS = 'list:add_all_list_items'

export const addListItem = listitem => {
	return dispatch => {
		axios.post('http://localhost:8000/list_items/', {
		title: listitem.title,
		description: listitem.description
	})
		.then(res => {
			dispatch({
				type: ADD_ITEM,
				payload: res.data
			})
		})
		.catch(err => {
			console.log('Create list item error', err)
		})
	}	
}

export const deleteListItem = id => {
	return dispatch => {
		axios.delete(`http://localhost:8000/list_items/${id}`)
		.then(res => {
			dispatch({
				type: DELETE_ITEM,
				payload: {id: id}
			})
		})
		.catch(err => {
			console.log('Delete list item error', err)
		})
	}	
}

export const addListItems = payload => {
	return {
		type: ADD_ALL_ITEMS,
		payload: payload
	}
}