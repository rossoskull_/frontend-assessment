import React from 'react'
import { shallow } from 'enzyme'
import App from './App'

describe('BucketList App', () => {
	it('Shows loading when the list is loading', () => {
		const props = {
			loading: true
		}

		const wrapper = shallow(<App props={loading: true} />)
		expect(wrapper.find('.loading').length).toEqual(1)
	})
})