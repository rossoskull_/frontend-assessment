import React from 'react'
import { connect } from 'react-redux'
import { addListItem } from '../../actions/ListItemsActions.js'

class CreateItem extends React.Component {

	constructor(props) {
		super(props)
		this.state = {
			createItemTitle: '',
			createItemDesc: ''
		}
	}

	handleChange = e => {
		this.setState({ [e.target.name]: e.target.value})
	}

	handleSubmit = e => {
		e.preventDefault()
		this.props.addListItem({
			title: this.state.createItemTitle,
			description: this.state.createItemDesc
		})
	}

	render() {
		return (
			<div id='create-item' className='card'>
				<div className='card-header'>
					<h2>Create List Item</h2>
				</div>
				<form onSubmit={this.handleSubmit}>
					<div className='card-body'>
						<div className='form-group'>
							<label>Title</label>
							<input className="form-control" type='text' name='createItemTitle' id='create-item-title' placeholder='Title' onChange={this.handleChange} />
						</div>

						<div className='form-group'>
							<label>Description</label>
							<input className="form-control" type='text' name='createItemDesc' id='create-item-desc' placeholder='Description' onChange={this.handleChange} />
						</div>						
					</div>

					<div className='card-footer'>
						<input className='btn btn-outline-success btn-lg' type='submit' id='create-item-submit' value='Create' />
					</div>					
				</form>
			</div>
		)
	}
}

const mapActionsToProps = {
	addListItem: addListItem
}

export default connect(null, mapActionsToProps)(CreateItem)