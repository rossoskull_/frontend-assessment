import React from 'react'
import { connect } from 'react-redux'
import { deleteListItem } from '../../../actions/ListItemsActions'

const ListItem = ({data, deleteListItemProp}) => {

	const onDelete = () => {
		deleteListItemProp(data.id)
	}

	return (
		<div className='list-item card  mb-3' key={data.id}>
			<h4 className='list-item-title card-header'>{data.title}</h4>
			<p className='list-item-desc card-body'>{data.description}</p>
			<div className='card-footer'>
				<button onClick={onDelete} type='button' className='btn btn-outline-danger btn-lg'>Delete</button>
			</div>
		</div>
	)
}

const mapActionsToProps = {
	deleteListItemProp: deleteListItem
}

export default connect(null, mapActionsToProps)(ListItem)