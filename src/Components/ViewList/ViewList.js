import React from 'react'
import ListItem from './ListItem/ListItem'

const ViewList = props => {

	return (
		<div id='bucket-list' className='mt-5'>

			<h3>Your list</h3>

			{props.list ? props.list.map(listItemData => {
				return (
					<ListItem data={listItemData} key={listItemData.id} />
				)	
			}) : (
				<h3>There are no items in the bucket list, go add some!</h3>
			)}
		</div>
	)
}

export default ViewList