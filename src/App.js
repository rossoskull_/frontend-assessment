import React from 'react';
import ViewList from './Components/ViewList/ViewList'
import CreateItem from './Components/CreateItem/CreateItem'
import axios from 'axios'
import { addListItems } from './actions/ListItemsActions.js'
import './App.css';

import { connect } from 'react-redux'

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    axios.get('http://localhost:8000/list_items')
      .then(res => {
        this.props.addListItems(res.data)
      })
      .catch(err => {
        console.log('Fetch error', err)
      })
  }



  render() {

    let { list, loading } = this.props

    return (
      <div className="App container">
        <h1 id='title' className='mb-5'><u>BucketList</u></h1>
        <CreateItem />
        {!loading ? (
          <ViewList list={list} />
        ) : (<h2 id='loading'>Loading...</h2>)}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    list: state.list.list,
    loading: state.list.loading
  }
}

const mapActionToProps = {
  addListItems: addListItems
}

export default connect(mapStateToProps, mapActionToProps)(App);
