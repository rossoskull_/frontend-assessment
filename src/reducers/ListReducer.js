import { ADD_ITEM, DELETE_ITEM, ADD_ALL_ITEMS } from '../actions/ListItemsActions'

let initState = {list: [], loading: true}

export default (state = initState, {type, payload}) => {
	switch (type) {
		case ADD_ITEM:
			return {list: [payload, ...state], loading: false}

		case ADD_ALL_ITEMS:
			return {list: [...payload], loading: false}

		case DELETE_ITEM:
			return {list: state.list.filter(s => s.id !== payload.id), loading: false}
		
		default: 
			return state
	}
}