import puppeteer from 'puppeteer'

const appUrl = 'http://localhost:3000'

let browser
let page

beforeAll(async () => {
	browser = await puppeteer.launch({})
	page = await browser.newPage()
})

describe('Title: Bucket List', () => {
	test('Bucket List', async () => {
		await page.goto(`${appUrl}/`)
		await page.waitForSelector('#title')
		const result = await page.evaluate(() => {
			return document.querySelector('#title').innerText
		})

		expect(result).toEqual('BucketList')
	})
})

afterAll(() => {
	browser.close()
})

// ViewList
beforeAll(async () => {
	browser = await puppeteer.launch({})
	page = await browser.newPage()
})

describe('View Bucket List', () => {
	// Dummy list should be loaded
	test('Loads dummy data', async () => {
		await page.goto(`${appUrl}/`)
		await page.waitForSelector('#bucket-list')
		const result = await page.evaluate(() => {
			return [ ...document.querySelectorAll('.list-item')].map(e => e.innerText)
		})

		expect(result.length).toEqual(3)
	})
})

afterAll(() => {
	browser.close()
})

// List Item
beforeAll(async () => {
	browser = await puppeteer.launch({})
	page = await browser.newPage()
})

describe('List Item Renders', () => {
	// Every list item should have a title
	test('Title Renders', async () => {
		await page.goto(`${appUrl}/`)
		await page.waitForSelector('.list-item')

		const result = await page.evaluate(() => {
			return [...document.querySelectorAll('.list-item-title')].map(e => e.innerText)
		})

		expect(result.length).toEqual(3)
	})

	// Every list item should have a description
	test('Description Renders', async () => {
		await page.goto(`${appUrl}/`)
		await page.waitForSelector('#bucket-list')

		const result = await page.evaluate(() => {
			return [...document.querySelectorAll('.list-item-desc')].map(e => e.innerText)
		})

		expect(result.length).toEqual(3)
	})
})

afterAll(() => {
	browser.close()
})


// CreateItem
beforeAll(async () => {
	browser = await puppeteer.launch({})
	page = await browser.newPage() 
})

describe('Create List Item Form', () => {
	test('List Item Title Field', async () => {
		await page.goto(`${appUrl}/`)
		await page.waitForSelector('#create-item')

		const result = await page.evaluate(() => {
			return document.querySelector('#create-item-title').placeholder
		})

		expect(result).toEqual('Title')
	})

	test('List Item Description Field', async () => {
		await page.goto(`${appUrl}/`)
		await page.waitForSelector('#create-item')

		const result = await page.evaluate(() => {
			return document.querySelector('#create-item-desc').placeholder
		})

		expect(result).toEqual('Description')
	})

	test('List Item Submit Button', async () => {
		await page.goto(`${appUrl}/`)
		await page.waitForSelector('#create-item')

		const result = await page.evaluate(() => {
			return document.querySelector('#create-item-submit').value
		})

		expect(result).toEqual('Create')
	})
})

afterAll(() => {
	browser.close()
})